#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <termios.h>

#define SWAP(x,y) {changed=true;x^=y;y^=x;x^=y;}
#define COMBINE(x,y) if(x&&y&&x==y){changed=true;x*=2;y=0;}
#define NEWTILE (rand()%10 ? 2 : 4)
#define ITERG for(I x=0;x<4;x++)for(I y=0;y<4;++y)
typedef unsigned int I;
struct termios orig_termios;

void disableRawMode() {if (tcsetattr(0, TCSAFLUSH, &orig_termios) == -1) exit(1);}
void enableRawMode() {
    if (tcgetattr(0, &orig_termios) == -1) exit(1);
    atexit(disableRawMode);
    struct termios raw = orig_termios;
    raw.c_iflag &= ~(BRKINT | ICRNL | INPCK | ISTRIP | IXON);
    raw.c_oflag &= ~(OPOST);
    raw.c_cflag |= (CS8);
    raw.c_lflag &= ~(ECHO | ICANON | IEXTEN | ISIG);
    if (tcsetattr(0, TCSAFLUSH, &raw) == -1) exit(1);
}

I game[4][4] = {{0}};
bool changed = true; // did the game state changed?

void display_game() {
	printf("\033[2J\033[H\r\n");
	for (I x=0;x<4;++x) {
		for (I y=0;y<4;++y) {
			if (game[x][y])
				 printf(" %4d", game[x][y]);
			else printf("     ");
		}
		printf("\r\n\r\n");
	}
}
void spawn_tile(){I x,y;do{x=rand()%4;y=rand()%4;}while(game[x][y]!=0);game[x][y]=NEWTILE;}
bool is_full() {ITERG if(!game[x][y])return 0;return 1;}
void swap_empty_tiles(I *a, I *b, I *c, I *d) {
	for (I i=0;i<4;++i) {
		if (!(*c) && *d) SWAP(*c,*d); 
		if (!(*b) && *c) SWAP(*b,*c);
		if (!(*a) && *b) SWAP(*a,*b);
	}
}
void add_tiles(I *a, I *b, I* c, I* d) {
	swap_empty_tiles(a,b,c,d);
	COMBINE(*a,*b);COMBINE(*b,*c);COMBINE(*c,*d);
	swap_empty_tiles(a,b,c,d);
}

void go_up() {
	for(int i = 0; i<4; ++i)add_tiles(&game[0][i],&game[1][i],
									  &game[2][i],&game[3][i]);
}
void go_down() {
	for(int i = 0; i<4; ++i)add_tiles(&game[3][i],&game[2][i],
									  &game[1][i],&game[0][i]);
}
void go_left() {
	for(int i = 0; i<4; ++i)add_tiles(&game[i][0],&game[i][1],
									  &game[i][2],&game[i][3]);
}
void go_right() {
	for(int i = 0; i<4; ++i)add_tiles(&game[i][3],&game[i][2],
									  &game[i][1],&game[i][0]);
}

int main() {
	enableRawMode();
	srand(time(NULL));
	
	spawn_tile();
	char c;
	while (!is_full()) {
		if (changed){ spawn_tile(); changed = false; }
		display_game();
		
		c = 0;
		printf("? ");
	_2048_getc:
		c = getchar();
		switch (c) {
		case 'k': go_up(); break;
		case 'j': go_down(); break;
		case 'h': go_left(); break;
		case 'l': go_right(); break;
		case 'q': return 1; break;
		default: goto _2048_getc;
		}
	}
	printf("Game finished.\r\n"); getchar();
	disableRawMode();
	return 0;
}
